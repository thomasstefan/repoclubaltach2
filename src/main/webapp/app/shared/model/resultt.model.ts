import { ITeam } from 'app/shared/model/team.model';

export interface IResultt {
  id?: number;
  opponent?: string;
  score?: string;
  team?: ITeam;
}

export const defaultValue: Readonly<IResultt> = {};
