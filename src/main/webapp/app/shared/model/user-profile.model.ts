export interface IUserProfile {
  id?: number;
  name?: string;
  phoneNo?: string;
}

export const defaultValue: Readonly<IUserProfile> = {};
