import { IPlayer } from 'app/shared/model/player.model';
import { IResultt } from 'app/shared/model/resultt.model';
import { IClub } from 'app/shared/model/club.model';

export interface ITeam {
  id?: number;
  teamName?: string;
  players?: IPlayer[];
  resultts?: IResultt[];
  club?: IClub;
}

export const defaultValue: Readonly<ITeam> = {};
