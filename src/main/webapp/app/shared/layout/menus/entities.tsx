import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown icon="th-list" name="Entities" id="entity-menu">
    <MenuItem icon="asterisk" to="/club">
      Club
    </MenuItem>
    <MenuItem icon="asterisk" to="/team">
      Team
    </MenuItem>
    <MenuItem icon="asterisk" to="/player">
      Player
    </MenuItem>
    <MenuItem icon="asterisk" to="/resultt">
      Resultt
    </MenuItem>
    <MenuItem icon="asterisk" to="/user-profile">
      User Profile
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
