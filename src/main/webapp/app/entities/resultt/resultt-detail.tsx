import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './resultt.reducer';
import { IResultt } from 'app/shared/model/resultt.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IResulttDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ResulttDetail = (props: IResulttDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { resulttEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Resultt [<b>{resulttEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="opponent">Opponent</span>
          </dt>
          <dd>{resulttEntity.opponent}</dd>
          <dt>
            <span id="score">Score</span>
          </dt>
          <dd>{resulttEntity.score}</dd>
          <dt>Team</dt>
          <dd>{resulttEntity.team ? resulttEntity.team.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/resultt" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/resultt/${resulttEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ resultt }: IRootState) => ({
  resulttEntity: resultt.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ResulttDetail);
