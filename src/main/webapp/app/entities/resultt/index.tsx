import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Resultt from './resultt';
import ResulttDetail from './resultt-detail';
import ResulttUpdate from './resultt-update';
import ResulttDeleteDialog from './resultt-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ResulttDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ResulttUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ResulttUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ResulttDetail} />
      <ErrorBoundaryRoute path={match.url} component={Resultt} />
    </Switch>
  </>
);

export default Routes;
