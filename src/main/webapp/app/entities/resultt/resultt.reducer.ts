import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IResultt, defaultValue } from 'app/shared/model/resultt.model';

export const ACTION_TYPES = {
  FETCH_RESULTT_LIST: 'resultt/FETCH_RESULTT_LIST',
  FETCH_RESULTT: 'resultt/FETCH_RESULTT',
  CREATE_RESULTT: 'resultt/CREATE_RESULTT',
  UPDATE_RESULTT: 'resultt/UPDATE_RESULTT',
  DELETE_RESULTT: 'resultt/DELETE_RESULTT',
  RESET: 'resultt/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IResultt>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ResulttState = Readonly<typeof initialState>;

// Reducer

export default (state: ResulttState = initialState, action): ResulttState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_RESULTT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_RESULTT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_RESULTT):
    case REQUEST(ACTION_TYPES.UPDATE_RESULTT):
    case REQUEST(ACTION_TYPES.DELETE_RESULTT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_RESULTT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_RESULTT):
    case FAILURE(ACTION_TYPES.CREATE_RESULTT):
    case FAILURE(ACTION_TYPES.UPDATE_RESULTT):
    case FAILURE(ACTION_TYPES.DELETE_RESULTT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_RESULTT_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_RESULTT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_RESULTT):
    case SUCCESS(ACTION_TYPES.UPDATE_RESULTT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_RESULTT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/resultts';

// Actions

export const getEntities: ICrudGetAllAction<IResultt> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_RESULTT_LIST,
    payload: axios.get<IResultt>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IResultt> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_RESULTT,
    payload: axios.get<IResultt>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IResultt> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_RESULTT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IResultt> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_RESULTT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IResultt> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_RESULTT,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
