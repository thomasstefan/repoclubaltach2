package at.fhv.itm19.st.web.rest;

import at.fhv.itm19.st.domain.Resultt;
import at.fhv.itm19.st.repository.ResulttRepository;
import at.fhv.itm19.st.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link at.fhv.itm19.st.domain.Resultt}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ResulttResource {

    private final Logger log = LoggerFactory.getLogger(ResulttResource.class);

    private static final String ENTITY_NAME = "resultt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ResulttRepository resulttRepository;

    public ResulttResource(ResulttRepository resulttRepository) {
        this.resulttRepository = resulttRepository;
    }

    /**
     * {@code POST  /resultts} : Create a new resultt.
     *
     * @param resultt the resultt to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new resultt, or with status {@code 400 (Bad Request)} if the resultt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/resultts")
    public ResponseEntity<Resultt> createResultt(@Valid @RequestBody Resultt resultt) throws URISyntaxException {
        log.debug("REST request to save Resultt : {}", resultt);
        if (resultt.getId() != null) {
            throw new BadRequestAlertException("A new resultt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Resultt result = resulttRepository.save(resultt);
        return ResponseEntity.created(new URI("/api/resultts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /resultts} : Updates an existing resultt.
     *
     * @param resultt the resultt to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated resultt,
     * or with status {@code 400 (Bad Request)} if the resultt is not valid,
     * or with status {@code 500 (Internal Server Error)} if the resultt couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/resultts")
    public ResponseEntity<Resultt> updateResultt(@Valid @RequestBody Resultt resultt) throws URISyntaxException {
        log.debug("REST request to update Resultt : {}", resultt);
        if (resultt.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Resultt result = resulttRepository.save(resultt);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, resultt.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /resultts} : get all the resultts.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of resultts in body.
     */
    @GetMapping("/resultts")
    public ResponseEntity<List<Resultt>> getAllResultts(Pageable pageable) {
        log.debug("REST request to get a page of Resultts");
        Page<Resultt> page = resulttRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /resultts/:id} : get the "id" resultt.
     *
     * @param id the id of the resultt to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the resultt, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/resultts/{id}")
    public ResponseEntity<Resultt> getResultt(@PathVariable Long id) {
        log.debug("REST request to get Resultt : {}", id);
        Optional<Resultt> resultt = resulttRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(resultt);
    }

    /**
     * {@code DELETE  /resultts/:id} : delete the "id" resultt.
     *
     * @param id the id of the resultt to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/resultts/{id}")
    public ResponseEntity<Void> deleteResultt(@PathVariable Long id) {
        log.debug("REST request to delete Resultt : {}", id);
        resulttRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
