package at.fhv.itm19.st.repository;

import at.fhv.itm19.st.domain.Resultt;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Resultt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResulttRepository extends JpaRepository<Resultt, Long> {

}
