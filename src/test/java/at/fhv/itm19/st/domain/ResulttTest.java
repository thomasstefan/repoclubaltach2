package at.fhv.itm19.st.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import at.fhv.itm19.st.web.rest.TestUtil;

public class ResulttTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Resultt.class);
        Resultt resultt1 = new Resultt();
        resultt1.setId(1L);
        Resultt resultt2 = new Resultt();
        resultt2.setId(resultt1.getId());
        assertThat(resultt1).isEqualTo(resultt2);
        resultt2.setId(2L);
        assertThat(resultt1).isNotEqualTo(resultt2);
        resultt1.setId(null);
        assertThat(resultt1).isNotEqualTo(resultt2);
    }
}
