package at.fhv.itm19.st.web.rest;

import at.fhv.itm19.st.Clubaltach2App;
import at.fhv.itm19.st.domain.Resultt;
import at.fhv.itm19.st.repository.ResulttRepository;
import at.fhv.itm19.st.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static at.fhv.itm19.st.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ResulttResource} REST controller.
 */
@SpringBootTest(classes = Clubaltach2App.class)
public class ResulttResourceIT {

    private static final String DEFAULT_OPPONENT = "AAAAAAAAAA";
    private static final String UPDATED_OPPONENT = "BBBBBBBBBB";

    private static final String DEFAULT_SCORE = "AAAAAAAAAA";
    private static final String UPDATED_SCORE = "BBBBBBBBBB";

    @Autowired
    private ResulttRepository resulttRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restResulttMockMvc;

    private Resultt resultt;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ResulttResource resulttResource = new ResulttResource(resulttRepository);
        this.restResulttMockMvc = MockMvcBuilders.standaloneSetup(resulttResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Resultt createEntity(EntityManager em) {
        Resultt resultt = new Resultt()
            .opponent(DEFAULT_OPPONENT)
            .score(DEFAULT_SCORE);
        return resultt;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Resultt createUpdatedEntity(EntityManager em) {
        Resultt resultt = new Resultt()
            .opponent(UPDATED_OPPONENT)
            .score(UPDATED_SCORE);
        return resultt;
    }

    @BeforeEach
    public void initTest() {
        resultt = createEntity(em);
    }

    @Test
    @Transactional
    public void createResultt() throws Exception {
        int databaseSizeBeforeCreate = resulttRepository.findAll().size();

        // Create the Resultt
        restResulttMockMvc.perform(post("/api/resultts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultt)))
            .andExpect(status().isCreated());

        // Validate the Resultt in the database
        List<Resultt> resulttList = resulttRepository.findAll();
        assertThat(resulttList).hasSize(databaseSizeBeforeCreate + 1);
        Resultt testResultt = resulttList.get(resulttList.size() - 1);
        assertThat(testResultt.getOpponent()).isEqualTo(DEFAULT_OPPONENT);
        assertThat(testResultt.getScore()).isEqualTo(DEFAULT_SCORE);
    }

    @Test
    @Transactional
    public void createResulttWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resulttRepository.findAll().size();

        // Create the Resultt with an existing ID
        resultt.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResulttMockMvc.perform(post("/api/resultts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultt)))
            .andExpect(status().isBadRequest());

        // Validate the Resultt in the database
        List<Resultt> resulttList = resulttRepository.findAll();
        assertThat(resulttList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkOpponentIsRequired() throws Exception {
        int databaseSizeBeforeTest = resulttRepository.findAll().size();
        // set the field null
        resultt.setOpponent(null);

        // Create the Resultt, which fails.

        restResulttMockMvc.perform(post("/api/resultts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultt)))
            .andExpect(status().isBadRequest());

        List<Resultt> resulttList = resulttRepository.findAll();
        assertThat(resulttList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkScoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = resulttRepository.findAll().size();
        // set the field null
        resultt.setScore(null);

        // Create the Resultt, which fails.

        restResulttMockMvc.perform(post("/api/resultts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultt)))
            .andExpect(status().isBadRequest());

        List<Resultt> resulttList = resulttRepository.findAll();
        assertThat(resulttList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllResultts() throws Exception {
        // Initialize the database
        resulttRepository.saveAndFlush(resultt);

        // Get all the resulttList
        restResulttMockMvc.perform(get("/api/resultts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resultt.getId().intValue())))
            .andExpect(jsonPath("$.[*].opponent").value(hasItem(DEFAULT_OPPONENT)))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)));
    }
    
    @Test
    @Transactional
    public void getResultt() throws Exception {
        // Initialize the database
        resulttRepository.saveAndFlush(resultt);

        // Get the resultt
        restResulttMockMvc.perform(get("/api/resultts/{id}", resultt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resultt.getId().intValue()))
            .andExpect(jsonPath("$.opponent").value(DEFAULT_OPPONENT))
            .andExpect(jsonPath("$.score").value(DEFAULT_SCORE));
    }

    @Test
    @Transactional
    public void getNonExistingResultt() throws Exception {
        // Get the resultt
        restResulttMockMvc.perform(get("/api/resultts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResultt() throws Exception {
        // Initialize the database
        resulttRepository.saveAndFlush(resultt);

        int databaseSizeBeforeUpdate = resulttRepository.findAll().size();

        // Update the resultt
        Resultt updatedResultt = resulttRepository.findById(resultt.getId()).get();
        // Disconnect from session so that the updates on updatedResultt are not directly saved in db
        em.detach(updatedResultt);
        updatedResultt
            .opponent(UPDATED_OPPONENT)
            .score(UPDATED_SCORE);

        restResulttMockMvc.perform(put("/api/resultts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedResultt)))
            .andExpect(status().isOk());

        // Validate the Resultt in the database
        List<Resultt> resulttList = resulttRepository.findAll();
        assertThat(resulttList).hasSize(databaseSizeBeforeUpdate);
        Resultt testResultt = resulttList.get(resulttList.size() - 1);
        assertThat(testResultt.getOpponent()).isEqualTo(UPDATED_OPPONENT);
        assertThat(testResultt.getScore()).isEqualTo(UPDATED_SCORE);
    }

    @Test
    @Transactional
    public void updateNonExistingResultt() throws Exception {
        int databaseSizeBeforeUpdate = resulttRepository.findAll().size();

        // Create the Resultt

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResulttMockMvc.perform(put("/api/resultts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultt)))
            .andExpect(status().isBadRequest());

        // Validate the Resultt in the database
        List<Resultt> resulttList = resulttRepository.findAll();
        assertThat(resulttList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteResultt() throws Exception {
        // Initialize the database
        resulttRepository.saveAndFlush(resultt);

        int databaseSizeBeforeDelete = resulttRepository.findAll().size();

        // Delete the resultt
        restResulttMockMvc.perform(delete("/api/resultts/{id}", resultt.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Resultt> resulttList = resulttRepository.findAll();
        assertThat(resulttList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
